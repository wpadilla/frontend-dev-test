# Frontent Test

Esta es una prueba de evaluación para la posición de `Frontend Developer` en el departamento de desarrollo de la UNPHU.

Nota: Al completar la prueba, debes enviar un .zip con la solución a `mhernandez@unphu.edu.do`.

## Prueba

Se necesita una pagina web estática que contenga las siguientes secciones. Los requerimientos a seguir son los siguientes:

* Una página de login para introducir mi usuario y contraseña con un botón de "entrar" para acceder a las demas secciones.
* Al pulsar el boton de entrar, me debe redirigir a un dashboard (administrativo).
* El dashboard debe contener un menu (sidebar) con dos items: Agregar usuario y lista de usuarios.
* Al pulsar "Agregar usuario", debe dirigirme a un formulario donde puedo editar todos los campos de un usuario (detallados más abajo).
* Al pulsar "Guardar" (en el formulario) debe dirigirme a la lista de usuarios.
* En el sidebar, Al pulsar "Lista de usuarios", debe mostrar al menos 10 usuarios y sus respectivos campos.

Importante:
* No es necesario que los formularios (login o agregar usuario) tengan ninguna validación, todo en la pagina debe ser estático.
* La parte inicial del dashboard (luego del login) puede estar vacía.
* En la lista de usuarios, puedes usar una tabla, una lista, tarjetas o contenedores. Coloca los items a tu gusto.
* Puedes utilizar los colores de tu preferencia.

Observaciones:
* Puedes usar el framework con el que te sientas más cómodo, al igual que los colores y distribución de los elementos.
* No uses javascript, a menos que sea estrictamente necesario.
* Si piensas incluir animaciones, debes usar css para las mismas (no jquery).
* El uso de SASS sería un plus.

## Datos

Campos del formulario de usuario:
* Nombres
* Primer apellido
* Segundo apellido
* Cédula
* Edad
* Género
* Dirección
* Teléfono
* Correo electrónico
* Estado civil
* Tienes hijos?
* Fecha de nacimiento

Campos a mostrar:
* ID
* Nombre completo
* Correo electrónico
* Género